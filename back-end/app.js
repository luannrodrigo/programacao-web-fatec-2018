var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

//ligando a aplicação à rota
const testeRouter = require('./routes/teste');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/*
  mongodb:// protocolo de rede
  localhost -> nome do servidor, no caso a maquina local
  :27017 -> porta no qual o mongo fica escutando
  brecho-note -> nome do banco
*/

//midleware para conexão com o banco de dados
const db = require('./config/database');
// string de conexão
db('mongodb://127.0.0.1:27017/brecho-note');

app.use('/', indexRouter);
app.use('/users', usersRouter);

//associando a rota /ola
app.use('/ola', testeRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
