const mongoose = require('mongoose');

module.exports = function (uri) {
    mongoose.connect(uri, {useNewUrlParser: true});

    mongoose.connection.on('connected', function () {
        console.log(`Mongoose! conectado a ${uri}`);
    });

    mongoose.connection.on('disconnected', function () {
        console.log(`Mongoose! Desconectado de ${uri}`);
    });

    mongoose.connection.on('error', function () {
        console.log(`Mongoose! Err: não foi possivel conectar ao Mongodb em ${uri}. Motivo: ${erro}`);
    });

// capturando um sinal de encerramento (SINGINIT)
    process.on('SIGINT', function () {
        mongoose.connection.close(function () {
            console.log(`Mongoose desconectado da aplicação`)
            process.exit(0);
        });
    });
};