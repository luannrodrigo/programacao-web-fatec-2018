const express = require('express');
const router = express.Router();

//ligando a rota ao controller
const controller = require('../controllers/teste');

//chamando o metodo ola_mundo() a rota
router.get('/', controller().ola_mundo);

module.exports = router;