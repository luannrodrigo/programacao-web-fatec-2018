const Artigo = require('../models/Artigo')(/*Construtor*/);

module.exports = function () {
    const controller = {};
    //criando o create do CRUD
    controller.novo = function (req, res) {
        /*Cria um novo artigo a partir das informações
        * que vieram no corpo (body) da requisição*/

        const artigo = new Artigo(req.body);

        //Callback quando acabar da salvar
        artigo.save({
            function(erro){
                if(erro){ // erro ao salvar no banco
                    //    http 500: erro interno do servidor
                    res.send(500).end();
                }else{ // dados salvo com sucesso
                    //http 201: criado
                    res.send(201).end();
                }
            }
        });
    };


    return controller;
};
