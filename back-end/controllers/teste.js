module.exports = function () {
    const controller = {};

// inicializando um obj imutavel chamado controller com conteúdo vazio
    /*
        Toda função de controller tem pelo menos dois parametros
        1º parametro: req = requisão recibida
        2º paramentro: res = resposta para o cliente


     */


    controller.ola_mundo = function (req, res) {
        //enviando uma mensagem de resposta
        res.send("Olá Mundo").end();

    }
    return controller;
}