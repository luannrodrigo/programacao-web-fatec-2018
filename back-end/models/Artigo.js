const mongoose = require('mongoose');

module.exports = function () {
    const schema = mongoose.Schema({
        descricao: {
            type: String,
            required: true // O atributo nção pode ficar em branco
        },
        tipo: {
            type: String,
            required: true
        },
        tamanho: {
            type: String,
            required: true
        },
        cor: {
            type: String,
            required: true
        },
        marca: {
            type: String
        },
        linha: {
            type: String,
            required: true
        },
        origem: {
            type: String
        },
        estado_conservacao: {
            type: String,
            required: true
        },
        conjunto: {
            type: boolean
            required: true
        },
        data_compra: {
            type: Date,
            required: true,
            default: Date.now()
        },
        valor_compra: {
            type: number,
            required: true
        },
        data_venda: {
            type: Date
        },
        valor_venda: {
            type: Number
        }


    });
/* 1º Parametro: Nome do modelo
   2º Parametro: Esquela do modelo
   3º Parametro: Nome da coleção onde os obj serão armazenados
 */
    return mongoose.model('Artigo', schema, 'artigos');
}